=== SLP Storefront ===
Contributors: charlestonsw
Tags: woocommerce, woothemes, storefront, store locator
Requires at least: 4.1
Tested up to: 4.5.2
Stable tag: 2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
A child theme for WooThemes Storefront.   This adds layout and style rules used to create the StoreLocatorPlus.com website.
The theme is [bbPress](https://bbpress.org/) and [Store Locator Plus](http://storelocatorplus.com/) aware.

* Adds shortcode processing in the widget areas.
* Removes the homepage controls sections from the homepage.

== Installation ==

1. Go fetch and install the [WooThemes Storefront](http://www.woothemes.com/storefront/) theme, this is a child theme for that main theme:
1a. In your admin panel, go to Appearance -> Themes and click the Add New button.
1b. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.

2. Install the SLP Storefront theme and activate it:
2a. In your admin panel, go to Appearance -> Themes and click the Add New button.
2b. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.
2c. Click Activate to use your new theme right away.

== Change Log ==

= 2.0 =

* Compatible with Storefront 2.0.0

= 1.0 =

* Add homepage action boxes ruleset for divs with .home_actionblock set.
* Style the bbPress subscribe/unsubscribe button.
* Remove extra homepage control stuff.
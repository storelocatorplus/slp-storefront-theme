<?php
/**
 * Store Locator Plus Storefront setup functions
 *
 * @package slpstorefront
 */

/**
 * Assign the Boutique version to a var
 */
$theme 					= wp_get_theme( 'slpstorefront' );
$slpstorefront_version 	= $theme['Version'];
<?php

/**
 * Override the default footer.
 */
function storefront_credit() {
    ?>
    <div class="site-info">
        <?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
        <?php if ( apply_filters( 'storefront_credit_link', true ) ) { ?>
            <br /> <?php

	        $alt_text = __( 'Cyber Sprocket Labs' , 'slp-storefront' );
            printf(
                __( '%1$s augmented by %2$s.', 'slp-storefront' ), 'Storefront',
                "<a href='http://www.cybersprocket.com' alt='{$alt_text}' title='{$alt_text}' rel='designer' target='cyber-sprocket'>Cyber Sprocket Labs</a>"
            );
        } ?>
    </div>
<?php
}

/**
 * Override the default header
 *
 * @since 1.0.0
 */
function storefront_page_header() {
	?>
	<header class="entry-header">
		<?php
		the_title( '<h1 class="entry-title">', '</h1>' );
		?>
	</header><!-- .entry-header -->
	<?php
}

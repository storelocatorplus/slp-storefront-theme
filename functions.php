<?php
/**
 * Store Locator Plus Storefront
 *
 * @package slpstorefront
 */

/**
 * Initialize all the things.
 */
require get_stylesheet_directory() . '/inc/init.php';

/* Disable Akisment Debugging WTF */

add_filter( 'akismet_debug_log', '__return_false' );

/* Disable Woo renewal notices */
add_filter( 'woothemes_helper_display_renewals_notices' , '__return_false' );

/* Disable password change notice */
remove_action( 'after_password_reset', 'wp_password_change_notification' );

/*-----------------------------------------------------------------------------------*/
/* 1. Woo Shortcodes  */
/*-----------------------------------------------------------------------------------*/

// Replace WP autop formatting
if ( ! function_exists( 'woo_remove_wpautop' ) ) {
    function woo_remove_wpautop( $content ) {
        $content = do_shortcode( shortcode_unautop( $content ) );
        $content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );
        return $content;
    } // End woo_remove_wpautop()
}

// Enable shortcodes in widget areas
add_filter( 'widget_text', 'do_shortcode' );

function woo_shortcode_button( $atts, $content = null ) {
    extract( shortcode_atts( array( 'size' => '',
        'style' => '',
        'bg_color' => '',
        'color' => '',
        'border' => '',
        'text' => '',
        'class' => '',
        'link' => '#',
        'window' => '' ), $atts ) );


    // Set custom background and border color
    $color_output = '';
    if ( $color ) {
        $preset_colors = array( 'red', 'orange', 'green', 'aqua', 'teal', 'purple', 'pink', 'silver' );
        if ( in_array( $color, $preset_colors ) ) {
            $class .= ' ' . $color;
        } else {
            if ( $border ){
                $border_out = $border;
            } else {
                $border_out = $color;
            }

            $color_output = 'style="background:' . esc_attr( $color ) . ';border-color:' . esc_attr( $border_out ) . '"';

            // add custom class
            $class .= ' custom';
        }
    } else {
        if ( $border )
            $border_out = $border;
        else
            $border_out = $bg_color;

        $color_output = 'style="background:' . esc_attr( $bg_color ) . ';border-color:' . esc_attr( $border_out ) . '"';

        // add custom class
        $class .= ' custom';
    }

    $class_output = '';

    // Set text color
    if ( $text ) $class_output .= ' dark';
    // Set class
    if ( $class ) $class_output .= ' '.$class;
    // Set Size
    if ( $size ) $class_output .= ' '.$size;
    // Set window target
    if ( $window ) $window = 'target="_blank" ';

    $output = '<a ' . $window . 'href="' . esc_attr( esc_url( $link ) ) . '" class="woo-sc-button' . esc_attr( $class_output ) . '" ' . $color_output . '><span class="woo-' . esc_attr( $style ) . '">' . wp_kses_post( woo_remove_wpautop( $content ) ) . '</span></a>';
    return $output;
} // End woo_shortcode_button()

add_shortcode( 'button', 'woo_shortcode_button' );